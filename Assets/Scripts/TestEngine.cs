using System;
using System.Collections;
using Svelto.ECS;
using UnityEngine;

public class TestEngine : IQueryingEntitiesEngine
{
    public IEntitiesDB entitiesDB { get; set; }

    readonly IEntityFactory _entityFactory;
    private IEntityFunctions _entityFunctions;
    
    public TestEngine(IEntityFactory entityFactory, IEntityFunctions entityFunctions)
    {
        _entityFactory = entityFactory;
        _entityFunctions = entityFunctions;
    }

    public void Ready()
    {
        for (int i = 0; i < 3; i++)
        {
            EntityStructInitializer z = _entityFactory.BuildEntity<ValueTypeTestEntityDescriptor>(new EGID((uint)i, GameGroups.ACTIVE));
        }
        
        Tick().Run();
    }


    IEnumerator Tick()
    {
        while (true)
        {
            var data = entitiesDB.QueryEntities<ValueTypeTestEntityViewStruct>(GameGroups.ACTIVE, out uint count);
            
            Debug.Log($"R {count}");
            
            yield return null;
        }
    }
}