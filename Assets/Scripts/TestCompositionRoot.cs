﻿using Svelto.Context;
using Svelto.ECS;
using Svelto.ECS.Schedulers.Unity;
using UnityEngine;

public class TestCompositionRoot : ICompositionRoot
{
    private EnginesRoot _enginesRoot;


    public void OnContextInitialized<T>(T contextHolder)
    {
        QualitySettings.vSyncCount = -1;


        _enginesRoot = new EnginesRoot(new UnityEntitySubmissionScheduler());

        var generateEntityFactory1 = _enginesRoot.GenerateEntityFactory();

        var entFunctions = _enginesRoot.GenerateEntityFunctions();
        _enginesRoot.AddEngine(new TestEngine(generateEntityFactory1, entFunctions));
        _enginesRoot.AddEngine(new TestRemoveEngine(generateEntityFactory1, entFunctions));

    }

    public void OnContextDestroyed()
    {
    }

    public void OnContextCreated<T>(T contextHolder)
    {
    }
}
