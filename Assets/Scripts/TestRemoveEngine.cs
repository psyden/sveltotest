using System.Collections;
using Svelto.ECS;
using Svelto.Tasks.Enumerators;
using UnityEngine;

public class TestRemoveEngine : IQueryingEntitiesEngine
{
    public IEntitiesDB entitiesDB { get; set; }

    readonly IEntityFactory _entityFactory;
    private IEntityFunctions _entityFunctions;
    
    public TestRemoveEngine(IEntityFactory entityFactory, IEntityFunctions entityFunctions)
    {
        _entityFactory = entityFactory;
        _entityFunctions = entityFunctions;
    }

    public void Ready()
    {
        Tuck().Run();
    }
    
    IEnumerator Tuck()
    {
        while (true)
        {
            var data = entitiesDB.QueryEntities<ValueTypeTestEntityViewStruct>(GameGroups.ACTIVE);

            foreach (ValueTypeTestEntityViewStruct entry in data)
            {
                _entityFunctions.RemoveEntity<ValueTypeTestEntityDescriptor>(entry.ID);
            }
                
            yield return null;
        }
    }
}