using Svelto.ECS;
using Svelto.ECS.Components;
using Svelto.ECS.Hybrid;

public class TestEntityDescriptor : GenericEntityDescriptor<TestEntityViewStruct>
{
}

public interface IComponent
{
}

public interface Component1 : IComponent
{
    ECSVector3 Position1 { get; set; }
}
public interface Component2 : IComponent
{
    ECSVector3 Position2 { get; set; }
}
public interface Component3 : IComponent
{
    ECSVector3 Position3 { get; set; }
}

public struct TestEntityViewStruct : IEntityViewStruct
{
    public Component1 C1;
    public Component2 C2;
    public Component3 C3;
    public EGID ID { get; set; }
}


public class TestImplementor :  Component1, Component2, Component3
{
    public ECSVector3 Position1 { get; set; }
    public ECSVector3 Position2 { get; set; }
    public ECSVector3 Position3 { get; set; }
}


public class ValueTypeTestEntityDescriptor : IEntityDescriptor
{

    public IEntityBuilder[] entitiesToBuild => EntityBuilders;
    
    static readonly IEntityBuilder[] EntityBuilders;
    
    static ValueTypeTestEntityDescriptor()
    {
        EntityBuilders = new IEntityBuilder[]
        {
            new EntityBuilder<ValueTypeTestEntityViewStruct>()
        };
    }
}

public struct  ValueTypeTestEntityViewStruct : IEntityStruct, INeedEGID
{
    public float Data1;
    public float Data2;
    public EGID ID { get; set; }
}
