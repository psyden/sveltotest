using Svelto.ECS;

public class GameGroups
{
    public static readonly ExclusiveGroup ACTIVE = new ExclusiveGroup();
    public static readonly ExclusiveGroup INACTIVE = new ExclusiveGroup();
}
